build:
	protoc -I . consignment/consignment.proto --micro_out=. --go_out=.
	protoc -I . vessel/vessel.proto --micro_out=. --go_out=.
	protoc -I . user/user.proto --micro_out=. --go_out=.
